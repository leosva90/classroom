# Clonar
Navegar por interfaz de usuario y abrir la consola donde se quiera clonar el proyecto, o por linea de comando utilizando el comando `cd /mi_direccion_de_recurso`
Luego en consola, usar los comandos:
```
git clone https://gitlab.com/mvalerio.6391/classroom.git
cd classroom
```
> Nota: necesario tener instalado [Git](https://git-scm.com/downloads "Git") y usuario en [Gitlab](https://gitlab.com/mvalerio.6391/classroom "Gitlab") con [contraseña](https://gitlab.com/-/profile/password/edit) 

> Configurar Git
```
git config --global user.name "mi-nombre"
git config --global user.email "mi-email@gmail.com"
```

## Ejercicio 1 (commits)
Consiste en crear un archivo dentro de la raiz del repositorio, con el nombre del integrante que desea participar, usando la rama principal del proyecto.
##### 1. Crear archivo
Windows: `type nul > mi-nombre.txt`
Linux: `touch mi-nombre.txt`
MacOS: `touch mi-nombre.txt`

O por interfaz de usuario, crear un nuevo archivo .txt con su nombre.
##### 2. Agregar cambio y subir
1. Agregar todos los cambios: `git add .`
2. Preparar el envio: `git commit -m "presente"`
3. Subir los cambios: `git push -u origin main`

## Ejercicio 2 (branches)
Consiste en crear una rama bifurcada de la principal y dentro del archivo con su nombre, colocar la fecha del dia en curso.
##### 1. Crear rama
1. Bajar los cambios de la rama actual: `git pull`
2. Crear la rama: `git branch mi-nombre`
3. Nos situamos sobre la rama: `git checkout mi-nombre`

> Nota: en este paso, crearemos una rama local

##### 2. Modificar el archivo
1. Por interfaz de usuario, abrimos nuestro archivo con cualquier editor de texto
2. Lo editamos colocando la fecha actual en su contenido
3. Guardamos y cerramos el archivo

##### 3. Agregar cambio y subir
1. Agregar todos los cambios: `git add .`
2. Preparar el envio: `git commit -m "agrego la fecha"`
3. Subir los cambios: `git push -u origin mi-nombre`

> Nota: en este paso, crearemos una nueva rama de desarrollo en la nube

##### 3. Unimos a la rama principal
1. Nos situamos sobre la rama destino: `git checkout main`
2. Creamos la union: `git merge mi-nombre`

##### 4. Agregar cambio y subir
1. Agregar todos los cambios: `git add .`
2. Preparar el envio: `git commit -m "unimos a la rama principal"`
3. Subir los cambios: `git push -u origin main`



## Ejemplo 1 (conflicto)
Cuando en 2 ramas, se producen cambios en un mismo archivo y las mismas lineas, es normal que aparezcan conflictos de codigo. En el cual aqui presentamos un caso.
##### 1. Creamos una rama
A partir del estado anterior de este texto, creamos una rama que se bifurco de la principal y generara cambios sobre estas lineas.
##### 2. Generamos lineas en archivo readme.md
Para este ejemplo agregamos las lineas que esta leyendo sobre la rama principal, para que entren en conflicto con la solución
